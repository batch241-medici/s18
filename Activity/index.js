function sumOfTwoNum (num1, num2) {
	console.log("Displayed sum of " + num1 + " and " + num2 +".");
	return num1 + num2;
}

let sum = sumOfTwoNum(5, 15);
console.log(sum);

function diffOfTwoNum(num1, num2) {
	console.log("Displayed difference of " + num1 + " and " + num2 + ".");
	return num1 - num2;
}
let diff = diffOfTwoNum(20, 5);
console.log(diff);

function prodOfTwoNum (num1, num2) {
	console.log("The product of " + num1 + " and " + num2 + ".");
	return num1 * num2;
}
let product = prodOfTwoNum(50, 10);
console.log(product);

function quotientOfTwoNum (num1, num2) {
	console.log("The quotient of " + num1 + " and " + num2 + ".");
	return num1 / num2;
}

let quotient = quotientOfTwoNum(50, 10);
console.log(quotient);

function areaOfCircle (radius) {
	console.log("The result of getting the area of a circle with " + radius + " radius.");
	return Math.PI * Math.pow(radius, 2);
}

let area = areaOfCircle(15);
console.log(area);

function averageOfNum (num1, num2, num3, num4) {
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 +  ", and " + num4 + ".");
	return (num1 + num2 + num3 + num4) / 4;
}

let average = averageOfNum(20, 40, 60, 80);
console.log(average);

function passingScore (num1, num2) {
	console.log("Is " + num1 + "/" + num2 + " a passing score?");
	let percentage = (num1/num2) * 100;
	let isPassed = percentage >= 75
	return isPassed;
}

let isPassingScore = passingScore(38, 50);
console.log(isPassingScore);