//alert("Hi!");
/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function

*/

/*
	Syntax:

		function functionName(parameter) {
				code block
		}
		functionName(argument)

*/

//name is called parameter
//"parameter" acts as a named variable/container that exists only inside of a function
//it is used to store information that is provided to a function when it is called/invoked

let name = "John";

function printName(name) {

	console.log("My Name is " + name);
}


//"Juan" and "Miah", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Miah");

//Variables can also be passed as an ARGUMENT
let userName = "Elaine";

printName(userName);

printName(name);
printName(); // function calling without arguments will result to undefined parameters.

//
function greeting() {
	console.log("Hello, User!");
}

greeting("Eric");

//Using Multiple Parameters
//Multiple "arguments" will corresponds to the number of "parameters" declared in a function in succeeding order.
//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.
//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.


function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " "+ lastName)
}

createFullName("Erven", "Joshua", "Cabral");
//"Erven" will be stored in the parameter "firstName"
//"Joshua" will be stored in the parameter "middleName"
//"Cabral" will be stored in the parameter "lastName"


//In JS, providing more/less arguments than the expected parameters will not return an error
// In other programming languages, this will return an error stating that "the expected number of argument do not match the number of parameters".

createFullName("Eric", "Andales");
createFullName("Roland", "John", "Doroteo", "Jane");

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini-Activity: 10:42
		1. Create a function which is able to able to receive data as an argument.
			-This function should be able to receive the name of your favorite superhero 
			-Display the name of your favorite superhero in the console.

	Kindly the ss in our gc.

*/

function printMyFavoriteSuperhero(superhero) {
	console.log("My favorite superhero is: " + superhero)

}

printMyFavoriteSuperhero("Cardo Dalisay");

//Return Statement
/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

*/


function returnFullName(firstName, middleName, lastName) {

	return firstName + " " + middleName + " " + lastName
	
	console.log("Can we print this message?");
}
// In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.
//Whatever value that is returned from the "returnFullName" function can be stored in a variable
let completeName = returnFullName("John", "Doe", "Smith");

//This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console.

// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

console.log(completeName);
// In this example, console.log() will print the returned value of the returnFullName() function.
console.log(returnFullName("John", "Doe", "Smith"));

completeName = returnFullName("Jeru", "Nebur", "Palma");
console.log(completeName);


//You can also create a variable inside the function to contain the result and return that variable instead.
function returnAddress(city, country) {

	let fullAddress = city + ", " + country;
	return fullAddress
}

let myAddress = returnAddress("Sta. Mesa, Manila", "Philippines");
console.log(myAddress);

//Consider the ff code.

/*
	Mini-Activity: 11:30AM
		1. Debug our code. So that the function will be able to return a value and save it in a variable.
*/

//When a function the only has console.log() to display its result it will return undefined instead.
/*
function printPlayerInformation(userName, level, job) {
	console.log("Username: " + userName);
	console.log("Level: " + level);
	console.log("Job: "+ job);

}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1); //undefined

*/
//returns undefined because printPlayerInformation returns nothing. It only logs the message in the console.
//You cannot save any value from printPlayerInfo() because it does not RETURN anything

function printPlayerInformation(userName, level, job) {
	
	//return "Username: " + userName + ", Level: " + level + ", Job: " + job
	let userInfo = "Username: " + userName + ", Level: " + level + ", Job: " + job
	return userInfo

}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1); 